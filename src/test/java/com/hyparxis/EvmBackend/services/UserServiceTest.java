package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.Repository.IRoleRepository;
import com.hyparxis.EvmBackend.Repository.IUserRepository;
import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.User;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestConstructor;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@DisplayName("UserService tests")
@ExtendWith(MockitoExtension.class)
class UserServiceTest {


    @Mock
    private final IRoleRepository roleRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private final IUserRepository userRepository;

    @InjectMocks
    private  RoleService roleService;
    @InjectMocks
    private UserService userService;

    private Role role;

    private User user;

    UserServiceTest() {
        roleRepository = null;
        userRepository = null;
    }


    @BeforeEach
    public void setupUser() {
        role = Role.builder()
                .name("User_Role")
                .build();
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user = User.builder()
                .username("CorSlagmolen")
                .email("Cor@Hyparxis.nl")
                .password("password123")
                .roles(roles)
                .build();
    }



    @Test
    void listAll() {
    }

    @Test
    void get() {
    }

    @Test
    void listRoles() {
    }

    @Test
    void addRoleToUser() {
    }

    @Test
    void delRoleFromUser() {
    }

    @Test
    void findByUserName() {
    }

    @Test
    void deleteUser() {
    }

    @Test
    void findAlluser() {
    }
}