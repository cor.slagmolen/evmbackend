package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.Repository.IRoleRepository;
import com.hyparxis.EvmBackend.model.Role;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@DisplayName("RoleService tests")
@RequiredArgsConstructor
@ExtendWith(MockitoExtension.class)
class RoleServiceTest {

    @Mock
    private IRoleRepository roleRepository;

    @InjectMocks
    private RoleService roleService;

    private Role role;

    @BeforeEach
    public void setupRole() {
        role = Role.builder()
                .name("User_Role")
                .build();
    }


    // JUnit test for saveEmployee method
    @DisplayName("JUnit test for saveRole method")
    @Test
    public void givenRoleObject_whenSaveRole_thenReturnRoleObject() {
        // given - precondition or setup

        given(roleRepository.save(role)).willReturn(role);

        System.out.println(roleRepository);
        System.out.println(roleService);

        // when -  action or the behaviour that we are going test
        Optional<Role> savedRole = roleService.addRole(role.getName());

        System.out.println(savedRole);
        // then - verify the output
        assertThat(savedRole).isNotNull();
    }

    @Test
    public void findRole() {
    }

    @Test
    public void findAll() {
        List<Role> roles = roleService.findAll();
        System.out.println(roles);

        assertThat(roles.isEmpty());
    }


    @ParameterizedTest
    @ValueSource(strings = {"Hello"})
    public void addRole(String usr) {

    }
}