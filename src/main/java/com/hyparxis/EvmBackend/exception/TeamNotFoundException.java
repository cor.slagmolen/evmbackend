package com.hyparxis.EvmBackend.exception;

public class TeamNotFoundException extends RuntimeException {

    public TeamNotFoundException(String team) {

        super(String.format("Team with Id %d not found", team));
    }
}
