package com.hyparxis.EvmBackend.exception;


public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String name) {

        super(String.format("User with Id %d not found", name));
    }
}

