package com.hyparxis.EvmBackend.exception;

    public class TeamRoleNotFoundException extends RuntimeException {

        public TeamRoleNotFoundException(String role) {

            super(String.format("Team role with Id %d not found", role));
        }
    }

