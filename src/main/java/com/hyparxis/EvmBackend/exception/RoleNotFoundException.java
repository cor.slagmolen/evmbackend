package com.hyparxis.EvmBackend.exception;

    public class RoleNotFoundException extends RuntimeException {

        public RoleNotFoundException(String role) {

            super(String.format("Role with Id %d not found", role));
        }
    }

