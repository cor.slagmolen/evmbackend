package com.hyparxis.EvmBackend.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoginDto {
    @NotBlank
    @Size(max = 20)
    private String username;


    @NotBlank
    @Size(max = 120)
    private String password;

}
