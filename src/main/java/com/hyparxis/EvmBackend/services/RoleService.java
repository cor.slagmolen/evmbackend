package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.Repository.IRoleRepository;
import com.hyparxis.EvmBackend.model.Role;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class RoleService implements IRoleService{

    @Autowired
    private IRoleRepository roleRepository;
    public RoleService(IRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Optional<Role> findByRoleName(String role) {
        Optional<Role> userRole = roleRepository.findByName(role);
        if(userRole == null){
            throw new ResourceNotFoundException("Role does not exist with given id:" + role );
        }else {

            return userRole;
        }
    }
    @Override
    public List<Role> findAll() {
        List<Role> userRoles = roleRepository.findAll();
        return userRoles;
    }
    @Override
    public Optional<Role> addRole(String  role) {

        Optional<Role> savedRole = roleRepository.findByName(role);
        if(savedRole != null && !savedRole.isEmpty()){
            throw new ResourceNotFoundException("Role already exist with given id:" + role );
        }else {
            Role newRole = new Role(role);
            return Optional.of(roleRepository.save(newRole));
        }

    }
    public Optional<Role> deleteRole(String role) {

        Optional<Role> roleToDel = roleRepository.findByName(role);
        if(roleToDel == null || roleToDel.isEmpty()){
            throw new ResourceNotFoundException("Role already exist with given id:" + role );
        }
        roleRepository.deleteById(roleToDel.get().getId());
        return roleToDel;
    }
}
