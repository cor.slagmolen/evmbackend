package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.User;

import java.util.List;

public interface IUserService {
    public List<User> listAll();
    public User getUserById(Long id);
    public User addRoleToUser(String newRole, String username);
    public User delRoleFromUser(String newRole, String username);
    public User addTeamRoleToUser(String newRole, String username);
    public User delTeamRoleFromUser(String newRole, String username);
    public User findByUserName(String username);
    public User deleteUser(String username);
    public List<User> findAllusers();
    public List<Role> listRoles();
    public User addTeamToUser(String team, String username);
    public User delTeamFromUser(String team, String username);
}
