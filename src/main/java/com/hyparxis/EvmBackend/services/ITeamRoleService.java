package com.hyparxis.EvmBackend.services;
import com.hyparxis.EvmBackend.model.TeamRole;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ITeamRoleService {
    List<TeamRole> findAll();
    public TeamRole deleteTeamRole(TeamRole role);
    public TeamRole addTeamRole(TeamRole role);
    public Optional<TeamRole> findByTeamRoleName(String role);
    public Optional<TeamRole> findByTeamRoleId(Integer id);

}
