package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.model.Team;

import java.util.List;

public interface ITeamService {


    Team findByTeamName(String teamName);

    Team findByTeamId(Integer id);

    Team addTeam(Team team);
    Team editTeam(Team team);

    List<Team> findAllTeams();
}
