package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.dto.LoginDto;

public interface IAuthService {
    String login(LoginDto loginDto);
}
