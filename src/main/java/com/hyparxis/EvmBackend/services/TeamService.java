package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.Repository.ITeamRepository;
import com.hyparxis.EvmBackend.model.Team;
import com.hyparxis.EvmBackend.model.User;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
@Slf4j
public class TeamService implements ITeamService{

    @Autowired
    private ITeamRepository teamRepository;

    @Override
    public Team findByTeamName(String teamName){

        return teamRepository.findByTeamName(teamName).orElse(null);
    }
    @Override
    public Team findByTeamId(Integer id){

        return teamRepository.findById(id).orElse(null);
    }

    @Override
    public Team addTeam(Team team) {
        return teamRepository.save(team);
    }
    @Override
    public Team editTeam(Team team) {
        return teamRepository.save(team);
    }
    @Override
    public List<Team> findAllTeams() {
        return teamRepository.findAll();
    }
}
