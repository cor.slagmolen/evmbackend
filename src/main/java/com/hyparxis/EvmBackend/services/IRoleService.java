package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.model.Role;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IRoleService {
    List<Role> findAll();
    public Optional<Role> deleteRole(String role);
    public Optional<Role> addRole(String role);
    public Optional<Role> findByRoleName(String role);

}
