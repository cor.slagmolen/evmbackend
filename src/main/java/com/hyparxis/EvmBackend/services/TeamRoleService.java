package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.Repository.IRoleRepository;
import com.hyparxis.EvmBackend.Repository.ITeamRoleRepository;
import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.Team;
import com.hyparxis.EvmBackend.model.TeamRole;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class TeamRoleService implements ITeamRoleService{

    @Autowired
    private ITeamRoleRepository teamRoleRepository;
    public TeamRoleService(ITeamRoleRepository teamRoleRepository) {
        this.teamRoleRepository = teamRoleRepository;
    }

    @Override
    public Optional<TeamRole> findByTeamRoleName(String role) {
        Optional<TeamRole> userTeamRole = teamRoleRepository.findByName(role);
        if(userTeamRole == null){
            throw new ResourceNotFoundException("Team role does not exist with given id:" + role );
        }else {

            return userTeamRole;
        }
    }
    public Optional<TeamRole> findByTeamRoleId(Integer id) {
        Optional<TeamRole> userTeamRole = teamRoleRepository.findById(id);
        if(userTeamRole == null){
            throw new ResourceNotFoundException("Team role does not exist with given id:" + id );
        }else {

            return userTeamRole;
        }
    }
    @Override
    public List<TeamRole> findAll() {
        List<TeamRole> userTeamRoles = teamRoleRepository.findAll();
        return userTeamRoles;
    }
    @Override
    public TeamRole addTeamRole(TeamRole  teamRole) {

        Optional<TeamRole> savedTeamRole = teamRoleRepository.findByName(teamRole.name);
        if(savedTeamRole != null && !savedTeamRole.isEmpty()){
            throw new ResourceNotFoundException("Team role already exist with given name:" + teamRole.name );
        }else {
            return teamRoleRepository.save(teamRole);
        }

    }
    public TeamRole editTeamRole(TeamRole teamRole) {
        return teamRoleRepository.save(teamRole);
    }
    @Override
    public TeamRole deleteTeamRole(TeamRole teamRole) {

        Optional<TeamRole> roleToDel = teamRoleRepository.findByName(teamRole.name);
        if(roleToDel == null || roleToDel.isEmpty()){
            throw new ResourceNotFoundException("Team role does not exist with given name:" + teamRole.name );
        }
        teamRoleRepository.deleteById(roleToDel.get().getId());
        return teamRole;
    }
}
