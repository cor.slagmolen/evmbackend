package com.hyparxis.EvmBackend.services;

import com.hyparxis.EvmBackend.Repository.IRoleRepository;
import com.hyparxis.EvmBackend.Repository.ITeamRepository;
import com.hyparxis.EvmBackend.Repository.ITeamRoleRepository;
import com.hyparxis.EvmBackend.Repository.IUserRepository;
import com.hyparxis.EvmBackend.exception.RoleNotFoundException;
import com.hyparxis.EvmBackend.exception.TeamNotFoundException;
import com.hyparxis.EvmBackend.exception.TeamRoleNotFoundException;
import com.hyparxis.EvmBackend.exception.UserNotFoundException;
import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.Team;
import com.hyparxis.EvmBackend.model.TeamRole;
import com.hyparxis.EvmBackend.model.User;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@Slf4j
public class UserService implements IUserService{

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private ITeamRoleRepository teamRoleRepository;
    @Autowired
    private ITeamRepository teamRepository;
    public UserService(IUserRepository userRepository,
                       IRoleRepository roleRepository,
                       ITeamRoleRepository teamRoleRepository,
                       PasswordEncoder passwordEncoder,
                       ITeamRepository teamRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.teamRoleRepository = teamRoleRepository;
        this.passwordEncoder = passwordEncoder;
        this.teamRepository = teamRepository;
    }

    @Override
    public List<User> listAll() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public List<Role> listRoles() {
        return roleRepository.findAll();
    }

    @Override
    public User addTeamToUser(String newTeam, String username) {
        {
            User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
            Team team = teamRepository.findByTeamName(newTeam).orElseThrow(() -> new TeamNotFoundException(newTeam));

            user.addTeam(team);
            return userRepository.save(user);
        }
    }

    @Override
    public User delTeamFromUser(String delTeam, String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        Team team = teamRepository.findByTeamName(delTeam).orElseThrow(() -> new TeamNotFoundException(delTeam));
        user.delTeam(team);
        return userRepository.save(user);
    }

    @Override
    public User addRoleToUser(String newRole, String username)
    {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        Role role = roleRepository.findByName(newRole).orElseThrow(() -> new RoleNotFoundException(newRole));

        user.addRole(role);
        return userRepository.save(user);
    }
    @Override
    public User delRoleFromUser(String newRole, String username)
    {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        Role roleUser = roleRepository.findByName(newRole).orElseThrow(() -> new RoleNotFoundException(newRole));
        user.delRole(roleUser);
        return userRepository.save(user);
    }
    @Override
    public User addTeamRoleToUser(String newRole, String username)
    {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        TeamRole role = teamRoleRepository.findByName(newRole).orElseThrow(() -> new TeamRoleNotFoundException(newRole));

        user.addTeamRole(role);
        return userRepository.save(user);
    }
    @Override
    public User delTeamRoleFromUser(String newRole, String username)
    {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        TeamRole roleUser = teamRoleRepository.findByName(newRole).orElseThrow(() -> new TeamRoleNotFoundException(newRole));
        user.delTeamRole(roleUser);
        return userRepository.save(user);
    }
    @Override
    public User findByUserName(String username){
        return userRepository.findByUsername(username).orElse(null);
    }

    public User deleteUser(String username){
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        userRepository.delete(user);
        return user;
    }
    @Override
    public List<User> findAllusers() {
        return userRepository.findAll();

    }

    }


