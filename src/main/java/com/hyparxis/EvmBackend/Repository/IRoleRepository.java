package com.hyparxis.EvmBackend.Repository;

import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IRoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByName(String role);
}