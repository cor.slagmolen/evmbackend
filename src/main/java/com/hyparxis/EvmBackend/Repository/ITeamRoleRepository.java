package com.hyparxis.EvmBackend.Repository;

import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.TeamRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ITeamRoleRepository extends JpaRepository<TeamRole, Integer> {

    Optional<TeamRole> findByName(String teamRole);
}