package com.hyparxis.EvmBackend.Repository;

import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.Team;
import com.hyparxis.EvmBackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ITeamRepository extends JpaRepository<Team, Integer> {

    Optional<Team> findByTeamName(String teamname);
}
