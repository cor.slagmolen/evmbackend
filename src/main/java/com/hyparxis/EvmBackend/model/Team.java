package com.hyparxis.EvmBackend.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Table(name = "teams")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 45)
    public String teamName;

    @Column(nullable = false, length = 45)
    public String teamPurpose;

    @Column(nullable = false, length = 45)
    public String organization;

    @Enumerated(EnumType.STRING)
    private TeamType teamType;



    // getters and setters are not shown for brevity
}
