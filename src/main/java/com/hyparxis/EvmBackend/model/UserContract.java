package com.hyparxis.EvmBackend.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Table(name = "user_contracts")
public class UserContract {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public Boolean employee;
    public LocalDate startDate;
    public LocalDate endDate;
    public UserStatus userStatus;
    public Float  hourlyRateExVat;
    public String currency;
    public String broker;
}
