package com.hyparxis.EvmBackend.model;

public enum TeamType {
    Feature,
    Component,
    Platform;
}
