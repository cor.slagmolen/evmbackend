package com.hyparxis.EvmBackend.model;

public enum UserStatus {
    Onboarding,
    Active,
    Inactive;
}
