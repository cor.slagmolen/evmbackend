package com.hyparxis.EvmBackend.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.*;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    private String username;

    @NotBlank
    @Size(max = 20)
    private String firstname;

    @NotBlank
    @Size(max = 20)
    private String lastname;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(max = 120)
    private String password;

    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(  name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(  name = "user_teams",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id"))
    private Set<Team> teams = new HashSet<>();

    @OneToOne
    @JoinColumn(name = "details_user_id")
    private DetailsUser detailsUser;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(  name = "user_team_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "teamRole_id"))
    private Set<TeamRole> teamRoles = new HashSet<>();


    public void addRole(Role role) {
        this.roles.add(role);
    }

    public void delRole(Role role) {
        this.roles.remove(role);
    }

    public void addTeam(Team team) {
        this.teams.add(team);
    }

    public void delTeam(Team team) {
        this.teams.remove(team);
    }
    public void addTeamRole(TeamRole teamRole) {
        this.teamRoles.add(teamRole);
    }

    public void delTeamRole(TeamRole teamRole) {
        this.teamRoles.remove(teamRole);
    }
    public User(String username, String firstname, String lastname, String email, String encode, Set<Role> roles) {
    }
}
