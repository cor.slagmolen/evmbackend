package com.hyparxis.EvmBackend.controllers;

import com.hyparxis.EvmBackend.model.Team;
import com.hyparxis.EvmBackend.model.TeamRole;
import com.hyparxis.EvmBackend.services.RoleService;
import com.hyparxis.EvmBackend.services.TeamRoleService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/teamrole")
public class TeamRoleController {
    @Autowired
    private TeamRoleService roleService;

    @PostMapping("/add")
    public ResponseEntity<?> addTeamRole(@RequestBody @Valid TeamRole teamRole) {
        Optional<TeamRole> checkRole = roleService.findByTeamRoleName(teamRole.getName());
        if ( checkRole == null || checkRole.isEmpty())
        {
            roleService.addTeamRole(teamRole);

            return new ResponseEntity<>(teamRole, HttpStatus.CREATED);

        }else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getTeam(@PathVariable Integer id) {
        return ResponseEntity.ok(roleService.findByTeamRoleId(id));
    }
    @PutMapping("/edit")
    public ResponseEntity<?> editTeamRole(@RequestBody @Valid TeamRole teamRole) {
        System.out.println(teamRole);
        if (roleService.findByTeamRoleId(teamRole.getId()) != null)
        {
            roleService.editTeamRole(teamRole);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(teamRole, HttpStatus.CONFLICT);
    }
    @PostMapping("/del")
    public ResponseEntity<?> deleteRole(@RequestBody @Valid TeamRole teamRole) {
        Optional<TeamRole> checkRole = roleService.findByTeamRoleName(teamRole.getName());
        if ( checkRole == null || checkRole.isEmpty())
        {
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        }else {
            roleService.deleteTeamRole(teamRole);

            return new ResponseEntity<>(teamRole, HttpStatus.GONE);
        }

    }
    @GetMapping("/allteamroles")
    public ResponseEntity<?> getAllTeamRoles() {
        return ResponseEntity.ok(roleService.findAll());
    }

}
