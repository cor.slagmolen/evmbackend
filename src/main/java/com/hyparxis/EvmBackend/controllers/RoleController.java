package com.hyparxis.EvmBackend.controllers;

import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.User;
import com.hyparxis.EvmBackend.services.RoleService;
import com.hyparxis.EvmBackend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @PostMapping("/add/{role}")
    public ResponseEntity<?> addRole(@PathVariable String role) {
        roleService.addRole(role);

        return new ResponseEntity<>(role, HttpStatus.CREATED);
    }
    @PostMapping("/del/{role}")
    public ResponseEntity<?> deleteRole(@PathVariable String role) {
        roleService.deleteRole(role);

        return new ResponseEntity<>(role, HttpStatus.GONE);
    }

}
