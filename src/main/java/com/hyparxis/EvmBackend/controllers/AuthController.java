package com.hyparxis.EvmBackend.controllers;

import com.hyparxis.EvmBackend.Repository.IRoleRepository;
import com.hyparxis.EvmBackend.Repository.IUserRepository;
import com.hyparxis.EvmBackend.dto.LoginDto;
import com.hyparxis.EvmBackend.dto.SignupDto;
import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.User;
import com.hyparxis.EvmBackend.responses.JwtResponse;
import com.hyparxis.EvmBackend.security.JwtTokenProvider;
import com.hyparxis.EvmBackend.security.UserDetailsImpl;
import com.hyparxis.EvmBackend.services.IAuthService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class    AuthController {

    Logger logger = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IAuthService authService;

    @Autowired
    IRoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtTokenProvider jwtUtils;

    @PostMapping("/login")
    public ResponseEntity<?> authenticate(@Valid @RequestBody LoginDto loginDto){
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        String token = authService.login(loginDto);

        map.clear();
        map.put("status", 1);
        map.put("message", "Token successfully created ");
        map.put("Token", token);
        map.put("DateTime", LocalDateTime.now() );
        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupDto signUpRequest) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        logger.info("User to be saved: " + signUpRequest);
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            map.clear();
            map.put("status", 0);
            map.put("message", "Username is already taken!");
            map.put("DateTime", LocalDateTime.now() );
            return new ResponseEntity<>(map, HttpStatus.CONFLICT);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            map.clear();
            map.put("status", 0);
            map.put("message", "Email is already taken!");
            map.put("DateTime", LocalDateTime.now() );
            return new ResponseEntity<>(map, HttpStatus.CONFLICT);
        }
        Set<Role> roles = new HashSet<>();

        Optional<Role> userRole = roleRepository.findByName("Role_User");
        if(userRole == null || userRole.isEmpty()){
            Role newRole = new Role("Role_User");
            Optional.of(roleRepository.save(newRole));
            Optional<Role> role = roleRepository.findByName("Role_User");
            roles.add(role.get());
        }else {

            roles.add(userRole.get());
        }


        // Create new user's account
        User user = new User();
        user.setUsername(signUpRequest.getUsername());
        user.setFirstname(signUpRequest.getFirstname());
        user.setLastname(signUpRequest.getLastname());
        user.setEmail(signUpRequest.getEmail());
        user.setPassword(encoder.encode(signUpRequest.getPassword()));
        user.setRoles(roles);
        userRepository.save(user);
        logger.info("User to be saved: " + user);

        map.clear();
        map.put("status", 1);
        map.put("message", "User successfully created ");
        map.put("DateTime", LocalDateTime.now() );
        return new ResponseEntity<>(map, HttpStatus.CREATED);

    }
}
