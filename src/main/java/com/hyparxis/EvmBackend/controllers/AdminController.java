package com.hyparxis.EvmBackend.controllers;

import com.hyparxis.EvmBackend.model.Role;
import com.hyparxis.EvmBackend.model.User;
import com.hyparxis.EvmBackend.services.IRoleService;
import com.hyparxis.EvmBackend.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/admin")
public class  AdminController {

    @Autowired
    private IUserService userService;

    @PutMapping("{username}/addrole/{role}")
    public ResponseEntity<?> addRoleToUser(@PathVariable String username, @PathVariable String role) {
        User user = userService.addRoleToUser(role, username);
        return ResponseEntity.ok(user);
    }
    @PutMapping("{username}/delrole/{role}")
    public ResponseEntity<?> deleteRolefromUser(@PathVariable String username, @PathVariable String role) {
        User user = userService.delRoleFromUser(role, username);
        return ResponseEntity.ok(user);
    }
    @PutMapping("{username}/addteamrole/{role}")
    public ResponseEntity<?> addTeamRoleToUser(@PathVariable String username, @PathVariable String role) {
        User user = userService.addTeamRoleToUser(role, username);
        return ResponseEntity.ok(user);
    }
    @PutMapping("{username}/delteamrole/{role}")
    public ResponseEntity<?> deleteTeamRolefromUser(@PathVariable String username, @PathVariable String role) {
        User user = userService.delTeamRoleFromUser(role, username);
        return ResponseEntity.ok(user);
    }
    @PutMapping("{username}/addteam/{team}")
    public ResponseEntity<?> addTeamToUser(@PathVariable String username, @PathVariable String team) {
        User user = userService.addTeamToUser(team, username);
        return ResponseEntity.ok(user);
    }
    @PutMapping("{username}/delteam/{team}")
    public ResponseEntity<?> deleteTeamfromUser(@PathVariable String username, @PathVariable String team) {
        User user = userService.delTeamFromUser(team, username);
        return ResponseEntity.ok(user);
    }
    @DeleteMapping("/deluser/{name}")
    public ResponseEntity<?> deleteUser(@PathVariable String name) {
        User user = userService.deleteUser(name);
        return ResponseEntity.ok(user);
    }

    @GetMapping("/allusers")
    public ResponseEntity<?> getAllUser() {
        return ResponseEntity.ok(userService.findAllusers());
    }



}
