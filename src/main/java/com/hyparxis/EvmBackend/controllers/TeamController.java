package com.hyparxis.EvmBackend.controllers;
import com.hyparxis.EvmBackend.model.Team;
import com.hyparxis.EvmBackend.services.ITeamService;
import com.hyparxis.EvmBackend.services.RoleService;
import com.hyparxis.EvmBackend.services.TeamService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/team")
public class TeamController {
    @Autowired
    private ITeamService teamService;

    @GetMapping("/allteams")
    public ResponseEntity<?> getAllTeam() {
        return ResponseEntity.ok(teamService.findAllTeams());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getTeam(@PathVariable Integer id) {
        return ResponseEntity.ok(teamService.findByTeamId(id));
    }
    @PostMapping("/add")
    public ResponseEntity<?> addTeam(@RequestBody @Valid Team team) {
        if (teamService.findByTeamName(team.getTeamName()) != null)
        {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        teamService.addTeam(team);

        return new ResponseEntity<>(team, HttpStatus.CREATED);
    }
    @PutMapping("/edit")
    public ResponseEntity<?> editTeam(@RequestBody @Valid Team team) {
        System.out.println(team);
        if (teamService.findByTeamId(team.getId()) != null)
        {
            teamService.editTeam(team);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(team, HttpStatus.CONFLICT);
    }


}
