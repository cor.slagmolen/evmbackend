package com.hyparxis.EvmBackend.security;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@AllArgsConstructor
@Configuration
public class SecurityConstants {
    public static final String SECRET = "4EAC633B6FB5BAEE33E1496DD2F914EAC633B6FB5BAEE33E1496DD2F91";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";

    public String getTokenSecret() {
        return this.SECRET;
    }

}
